<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SsoController;

Route::get('/', [SsoController::class, 'index']);
