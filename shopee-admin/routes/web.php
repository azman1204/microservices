<?php
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

// http://shopee-admin.test
Route::view('/', 'welcome');
Route::get('/user-list', [UserController::class, 'listUser']);
Route::get('/sso', [UserController::class, 'sso']);

Route::middleware(['pakguard'])->group(function() {
    // http://shopee-admin.test/dashboard
    Route::get('/dashboard', function() {
        echo "<h1>Dashboard</h1>";
    });
});


Route::get('/logout', function() {
    \Auth::logout();
    return redirect('/');
});
