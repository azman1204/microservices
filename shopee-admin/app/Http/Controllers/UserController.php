<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller {
    // list users
    function listUser() {
        $url = "http://shopee-portal.test/api/profile";
        $response = Http::get($url);
        //dd($response->object());
        return view('user_list', ['profile' => $response->object()]);
    }

    // terima data dari shopee-sso.test, then validate token
    function sso(Request $req) {
        $token = $req->token;
        $url = 'http://shopee-sso.test/api/validate-token?key=1234&token='.$token;
        $response = Http::get($url);
        $data = $response->object();

        if ($data->status == 'ok') {
            $user = User::where('email', $data->email)->first();
            \Auth::login($user);
            return redirect('/dashboard');
        } else {
            echo 'No permission';
        }
    }
}
