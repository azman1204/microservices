<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pengguna;
use Illuminate\Support\Str;

class SsoController extends Controller {
    function index(Request $req) {
        // 1. check ada cookie bernama my-token
        // 2. jika belum ada cookie, show login page
        // 3. selepas login, set new cookie
        // 4. redirect ke domain yg dia datang
        $domain = $req->domain;


        if ($req->cookie('my-token')) {
            echo "ada cookie";
            $token = request()->cookie('my-token');
            if (User::where('remember_token', $token)->first()) {
                // cookie valid
                $url = "http://$domain?token=$token";
                return redirect($url);
            } else {
                // cookie tak valid
                return redirect("/login");
            }
        } else {
            //echo "tiada cookie";
            $url = "http://$domain";
            session(['url' => $url]);
            return redirect("/login");
        }
    }

    function login() {
        return view('login');
    }

    function auth(Request $req) {
        $user_id = $req->user_id;
        $password = $req->password;
        $credentials = ['email' => $user_id, 'password' => $password];
        if (\Auth::attempt($credentials)) {
            // save cookie ke DB
            $user = User::where('email', $user_id)->first();
            $rand = Str::random(40);
            $user->remember_token = $rand;
            $user->save();
            $url = session('url') . "?token=$rand";
            return response("<script>location.href='$url'</script>")->cookie('my-token', $rand);
        } else {
            return view('login')->with('err', 'Maklumat salah');
        }
    }

    function validateToken(Request $req) {
        //return 'ok';
        $token = $req->token;
        $key = $req->key;
        if ($key == '1234') {
            $user = Pengguna::where('remember_token', $token)->first();
            if ($user) {
                return response()->json([
                    'status' => 'ok',
                    'email' => $user->email
                ],200);
            } else {
                return response()->json([
                    'status' => 'ko'
                ],200);
            }
        } else {
            return response()->json([
                'status' => 'ko'
            ],200);
        }
    }
}
