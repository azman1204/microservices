<?php

use App\Http\Controllers\Api\NewsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// http://shopee-admin.test/api/latest
Route::get('/latest', [NewsController::class, 'latest']);
