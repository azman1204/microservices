<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Pakguard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (! \Auth::check()) {
            // user belum login
            // domain = guna semasa shopee-sso.test redirect semula
            return redirect("http://shopee-sso.test?domain=shopee-admin.test/sso");
        }
        return $next($request);
    }
}
